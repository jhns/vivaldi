# vivaldi

Vivaldi is a fast, privacy-friendly and highly customizable web browser developed by the
norwegian company Vivaldi technologies.

[Vivaldi Website](https://vivaldi.com/)

## How to use

- Download the Vivaldi Debian package from the website: [Vivaldi Downloads](https://vivaldi.com/download/) <br>
- Place the Vivaldi Debian package in the Downloads folder in your home directory. <br>
- Clone this repository and change into the created folder. <br>
- Execute the vivaldi.sh script without sudo.

## Remove Vivaldi

In the unlikely event that you want to remove Vivaldi you can execute the vivaldi-remove.sh script without sudo.

## Themes

I've created a collection of themes matching the colors of some popular GTK+ themes. <br>
You can find it [here](https://themes.vivaldi.net/users/jhns).

## Notes

The use of this script is at your own risk. Root rights are not used if possible.
If you encounter problems using this script, opening an issue may be a good place to start.
